/*
 * Easing Functions - inspired from http://gizma.com/easing/
 * only considering the t value for the range [0, 1] => [0, 1]
 */


const easing = {
    // no easing, no acceleration
    linear: function (t) { return t },
    // accelerating from zero velocity
    easeInQuad: function (t) { return t*t },
    // decelerating to zero velocity
    easeOutQuad: function (t) { return t*(2-t) },
    // acceleration until halfway, then deceleration
    easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
    // accelerating from zero velocity 
    easeInCubic: function (t) { return t*t*t },
    // decelerating to zero velocity 
    easeOutCubic: function (t) { return (--t)*t*t+1 },
    // acceleration until halfway, then deceleration 
    easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
    // accelerating from zero velocity 
    easeInQuart: function (t) { return t*t*t*t },
    // decelerating to zero velocity 
    easeOutQuart: function (t) { return 1-(--t)*t*t*t },
    // acceleration until halfway, then deceleration
    easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
    // accelerating from zero velocity
    easeInQuint: function (t) { return t*t*t*t*t },
    // decelerating to zero velocity
    easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
    // acceleration until halfway, then deceleration 
    easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
  }


  const lazy = fn => {
    let called = false
    let ret
  
    return () => {
      if (!called) {
        called = true
        ret = fn()
      }
  
      return ret
    }
  }


let isApiSetUp = false

const loadGmapApi = (options) => {

    if (typeof document === 'undefined') {
      // Do nothing if run from server-side
      return
    }

    if (!isApiSetUp) {

      isApiSetUp = true
  
      const googleMapScript = document.createElement('SCRIPT')
  
      if (typeof options !== 'object') {
        throw new Error('options should  be an object')
      }
  
      options['callback'] = 'GMapsInit'

      let baseUrl = 'https://maps.googleapis.com/'
  
      let url = baseUrl + 'maps/api/js?' +
        Object.keys(options)
          .map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(options[key]))
          .join('&')
  
      googleMapScript.setAttribute('src', url)
      googleMapScript.setAttribute('async', '')
      googleMapScript.setAttribute('defer', '')

      document.head.appendChild(googleMapScript)
      
    } else {
      throw new Error('You already started the loading of google maps')
    }
}


  export default {
    easing,
    lazy,
    loadGmapApi
  }