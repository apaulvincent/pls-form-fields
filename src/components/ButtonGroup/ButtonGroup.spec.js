import { mount } from '@vue/test-utils'
import ButtonGroup from './ButtonGroup.vue'

describe('ButtonGroup Component', () => {

    let wrapper;

    beforeEach( () => {
        wrapper = mount(ButtonGroup, {
            propsData: {
                data: [1, 2, 3, 4, 5, 6, 7, 8],
                name: "item",
                cols: 6,
                col_md: 3,
                col_sm: 2,
                col_xs: 1,
                showmore: true
            }
        });
    })

    it('renders radio buttons', () => {
        expect( wrapper.contains('input[type="radio"]')).toBe(true)
    })

    it('highlights selected button', () => {

        const btn1 = wrapper.findAll('input[type="radio"]').at(0)
        const btn2 = wrapper.findAll('input[type="radio"]').at(2)

        btn1.setChecked();
        
        expect(wrapper.vm.value).toBe('1')
        
        btn2.setChecked();
        
        expect(wrapper.vm.value).toBe('3')
    })

    it('show more options on button click', () => {

        const btn = wrapper.findAll('.btn-col').at(6)
        const more = wrapper.find('.showmore-btn')

        expect(btn.isVisible()).toBe(false)

        more.trigger('click');

        expect(btn.isVisible()).toBe(true)
        
    })


})