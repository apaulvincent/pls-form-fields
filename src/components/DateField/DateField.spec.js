import { mount } from '@vue/test-utils'
import DateField from './DateField.vue'

describe('DateField Component', () => {

    let wrapper;

    beforeEach( () => {
        wrapper = mount(DateField, {
            propsData: {
                modelValue: '',
                name: 'date',
                maxDate: -18,
                minDate: -100,
            }
        });
    })

    it('renders date field', () => {
        expect(wrapper.contains('input') ).toBe(true)
    })

    it('it masks input date', () => {
        wrapper.setValue('0907199999')
        expect(wrapper.vm.value).toBe('09-07-1999')
    })

    // NOTE: Keydown doesn't seem to trigger where logic of filtering input happens.

    // it('it removes unwanted values', () => {
    //     wrapper.trigger('keydown')
    //     wrapper.setValue('asd')
    //     expect(wrapper.vm.value).toBe('dd-mm-yyyy')
    // })


})