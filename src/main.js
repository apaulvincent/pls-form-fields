import * as components from './components'
import utils from './utils'
 
const PLSFormFields = {

  install(Vue, options) {

    let gmapPromise = gmapApiPromise()

    Vue.mixin({
      created () {
        this.$gmapApiPromise = gmapPromise
      }
    })

    for (const componentName in components) {

      const component = components[componentName]

      Vue.component(component.name, component)

    }

  }

}


export default PLSFormFields


/**
 * Auto-register when Vue instance exists in the window and module system is not used
 */
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(PLSFormFields)
}

function gmapApiPromise () {

  function onApiLoaded () {
    return window.google
  }

  const gapis = [
    'AIzaSyDA1hc655kDhJWBUxC5yE46ky3A1Eq2SHg',
    'AIzaSyAQt6PO0VaNOn64d0FPhI-Q7Gbk1o6IXAs',
    'AIzaSyCxERIKlrqI4HL9pkPDL5IJta4-5iGPPnQ',
    'AIzaSyCnmXsqQt9mqgMKH9vcXiVBxeQettc53P0',
    'AIzaSyCqMQVkPiT_Ir1-5KneKiy1NHgFgvrrC20',
    'AIzaSyAcwVIWmJyO12neg_OPj8sEEQoCs4p8bAg',
    'AIzaSyBSaQId92yznp6EPH59WnlYugxR4R0My_Q',
    'AIzaSyCpYHZNIFYsw7FAzh43wcBCDk9nhtBok3g',
    'AIzaSyBXAkB0lOJIebMP9SiBKyMgsIYgcOmcw5E',
    'AIzaSyA9j86jZTXFFVvcEfMBiUkcAuGssZJUCJE'];

  return utils.lazy(() => {

        return new Promise((resolve, reject) => {
          
            try {

              window['GMapsInit'] = resolve

              utils.loadGmapApi({
                  key: gapis[1],
                  libraries: 'places',
                  region: 'AU'
              })

            } catch (err) {

              reject(err)

            }

        }).then(onApiLoaded)
  })

}