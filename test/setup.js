require('jsdom-global')()

global.expect = require('expect')

/* 
 * + window.Date = Date; 
 * Temporary workaround for successful testing.
 * NOTE: Paul, in the future try to remove this and check if the error persists.
 * ERROR: TypeError: Super expression must either be null or a function.
*/
window.Date = Date; 