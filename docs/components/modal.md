# `<modal/>`

Component structure and options

## Example

<br>
<Demo componentName="examples-modal-doc" />

## Usage

<<< @/docs/.vuepress/components/examples/modal-doc.vue


## Props / Options

### `freeze`
- type: `Boolean`


### `size`
- type: `String`
- default: 'lg'

preset sizes: xs | sm | md | lg

### `maxWidth`
- type: `Number`

custom style max-width