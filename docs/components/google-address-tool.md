# `<google-address-tool/>`

Component structure and options

## Example
Select in the google address tool below and see how it populates the rest of the fields.

<Demo componentName="examples-google-address-tool-doc" />

## Usage

<<< @/docs/.vuepress/components/examples/google-address-tool-doc.vue

## Props / Options

### `data`
- type: `[Array, Object]`
- required

Selection data

## Validation

See documentation of validation using [VeeValidate](https://baianat.github.io/vee-validate/), a powerful validation plugin for Vue.

```vue{6-7-8}
    <button-group 
            :data="[ 'Male', 'Female']"
            :cols="2"
            name="gender"
            v-model="gender"
            v-validate="'required'"
            data-vv-as="gender" />
    <div class="error-msg" v-show="errors.has('gender')">{{ errors.first('gender') }}</div>
```