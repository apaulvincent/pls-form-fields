# `<button-group/>`

Component for converting selection inputs into intuitive UI

## Example

<br>
<Demo componentName="examples-button-group-doc" />

## Usage

<<< @/docs/.vuepress/components/examples/button-group-doc.vue

## Props / Options

### `data`
- type: `[Array, Object]`
- required

Selection data

### `cols`
- type: `[String, Number]`
- default: 1

Default number of columns.


### `col-xs`
- type: `[String, Number]`

Number of columns on mobile.

### `col-sm`
- type: `[String, Number]`

Number of columns on tablet.

### `col-md`
- type: `[String, Number]`

Number of columns on desktop.

### `showmore`
- type: `Boolean`
- default: false

Add button to show / hide rows of options.

## Validation

See documentation of validation using [VeeValidate](https://baianat.github.io/vee-validate/), a powerful validation plugin for Vue.

```vue{6-7-8}
    <button-group 
            :data="[ 'Male', 'Female']"
            :cols="2"
            name="gender"
            v-model="gender"
            v-validate="'required'"
            data-vv-as="gender" />
    <div class="error-msg" v-show="errors.has('gender')">{{ errors.first('gender') }}</div>
```