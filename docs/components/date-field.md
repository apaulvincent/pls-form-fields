# `<date-field/>`

Component structure and options

## Example

<br>
<Demo componentName="examples-date-field-doc" />

## Usage

<<< @/docs/.vuepress/components/examples/date-field-doc.vue


## Props / Options

### `model-value`
- type: `String`

Use this for static values or use `v-model` instead.

### `name`
- type: `String`

Name for iput field.

### `min-date`
- type: `Number`
- default: `0`

Add or minus to current year, 0 is present date.


### `max-date`
- type: `Number`
- default: `100`

Add or minus to current year, 0 is present date.

```⚠️ WARNING: Max Date must be larger than Min Date! ```


### `class-name`
- type: `String`


### `placeholder`
- type: `String`


### `readonly`
- type: `String`



## Validation

See documentation of validation using [VeeValidate](https://baianat.github.io/vee-validate/), a powerful validation plugin for Vue.


```vue{7-8}
    <date-field
        name="date" 
        id="date" 
        v-model="date"
        :max-date="-18"
        :min-date="-100"
        v-validate="'required'" 
        data-vv-validate-on="change" />
    <div class="error-msg" v-show="errors.has('date')">{{ errors.first('date') }}</div>
```