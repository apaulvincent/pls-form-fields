# Getting Started

``` pls-form-fields ``` is a library of enhanced mobile friendly form inputs.

### Installation


```js
// install with yarn
yarn add pls-form-fields

// install with npm
npm i pls-form-fields
```


### Import then use

```js
// main.js
import PLSFields from "pls-form-fields"
Vue.use(PLSFields)
```